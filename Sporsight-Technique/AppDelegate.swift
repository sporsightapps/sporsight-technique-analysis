//
//  AppDelegate.swift
//  Sporsight-Technique
//
//  Created by Bryant Gray on 2/15/19.
//  Copyright © 2019 Sporsight. All rights reserved.
//

import UIKit
import RealmSwift
import MSAL

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        print(Realm.Configuration.defaultConfiguration.fileURL)
        
//        client = MSClient(applicationURLString: "https://sporsight-mobile.azurewebsites.net")
//
//        let logger = MSALLogger.init() as MSALLogger
//
//        /** When capturing log messages from MSAL you only need to capture either messages where
//         containsPII == YES or containsPII == NO, as log messages are duplicated between the
//         two, however the containsPII version might contain Personally Identifiable Information (PII)
//         about the user being logged in.
//         */
//
//        logger.setCallback { (logLevel, message, containsPII) in
//            if (!containsPII) {
//                print(message!)
//            }
//        }
//
//        // Push notification
//        let center = UNUserNotificationCenter.current()
//        center.requestAuthorization(options: [.badge, .alert, .sound]) { (granted, error) in
//            // Enable or disable features based on authorization
//        }
//        application.registerForRemoteNotifications()
        
        do {
            let _ = try Realm()
        } catch {
            print("Error initializing new realm, \(error)")
        }
        
        let isSignedIn = UserDefaults.standard.bool(forKey: "isSignedIn")
        print(isSignedIn)
        if !isSignedIn {
            perform(#selector(showSignInVC), with: nil, afterDelay: 0.01)
        }
        
        return true
    }
    
    @objc func showSignInVC() {
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainSB.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.window?.rootViewController?.present(vc, animated: true, completion: nil)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        print("Recieved callback!")
        
        MSALPublicClientApplication.handleMSALResponse(url)
        
        return true
    }

}

