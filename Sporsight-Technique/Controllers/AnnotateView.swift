//
//  AnnotateView.swift
//  Sporsight_Mobile
//
//  Created by Jack Mustacato on 6/19/18.
//  Copyright © 2018 Microsoft. All rights reserved.
//

import SpriteKit

enum AnnotationType {
    case arrow
    case circle
    case freehand
    case rectangle
}

class AnnotateView: SKScene {
    
    var annotationPath: SKShapeNode!
    var annotationType: AnnotationType!
    var firstCorner: CGPoint!
    var lastCorner: CGPoint!
    var arrowStart: CGPoint!
    var arrowEnd: CGPoint!
    var annotationColor: UIColor!
    var annotationPoints = [CGPoint]()
    var paths = [SKShapeNode]()
    
    func createAnnotationPath() {
        annotationPath = SKShapeNode()
        annotationPath.zPosition = 2
        annotationPath.strokeColor = annotationColor
        annotationPath.lineWidth = 5
        
        addChild(annotationPath)
        paths.append(annotationPath)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        createAnnotationPath()
        
        if annotationType == .freehand {
            annotationPoints.removeAll(keepingCapacity: true)
        }
        
        if let touch = touches.first {
            let location = touch.location(in: self)
            if annotationType == .arrow {
                arrowStart = location
                arrowEnd = location
            } else if annotationType == .rectangle || annotationType == .circle {
                firstCorner = location
                lastCorner = location
            } else if annotationType == .freehand {
                annotationPoints.append(location)
            }
            
            redrawAnnotationPath()
            annotationPath.alpha = 1
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let location = touch.location(in: self)
        
        if annotationType == .freehand {
            annotationPoints.append(location)
        } else if annotationType == .rectangle || annotationType == .circle || annotationType == .arrow {
            lastCorner = location
            arrowEnd = location
        }
        
        redrawAnnotationPath()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
//            print(location)
            if annotationType == .rectangle || annotationType == .circle || annotationType == .arrow {
                lastCorner = location
                arrowEnd = location
                redrawAnnotationPath()
            }
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesEnded(touches, with: event)
    }
    
    func redrawAnnotationPath() {
        var path: UIBezierPath = UIBezierPath()
        
        if annotationType == .arrow {
            path = UIBezierPath.bezierPathWithArrowFromPoint(startPoint: arrowStart, endPoint: arrowEnd, tailWidth: 1, headWidth: 25, headLength: 25)
        } else if annotationType == .rectangle || annotationType == .circle{
            var rectX: CGFloat = 0
            var rectY: CGFloat = 0
            let rectWidth = abs(lastCorner.x - firstCorner.x)
            let rectHeight = abs(lastCorner.y - firstCorner.y)

            if firstCorner.x < lastCorner.x && firstCorner.y < lastCorner.y {
                rectX = firstCorner.x
                rectY = firstCorner.y
            } else if firstCorner.x < lastCorner.x && firstCorner.y > lastCorner.y {
                rectX = firstCorner.x
                rectY = firstCorner.y - rectHeight
            } else if firstCorner.x > lastCorner.x && firstCorner.y < lastCorner.y {
                rectX = firstCorner.x - rectWidth
                rectY = firstCorner.y
            } else {
                rectX = firstCorner.x - rectWidth
                rectY = firstCorner.y - rectHeight
            }
            
            let rect = CGRect(x: rectX, y: rectY, width: rectWidth, height: rectHeight)
            
            if annotationType == .rectangle {
                path = UIBezierPath(roundedRect: CGRect(x: rectX, y: rectY, width: rectWidth, height: rectHeight), cornerRadius: 0)
            } else {
                path = UIBezierPath(ovalIn: rect)
            }
        } else if annotationType == .freehand {
            path.move(to: annotationPoints[0])

            for i in 1 ..< annotationPoints.count {
                path.addLine(to: annotationPoints[i])
            }
        }
        
        annotationPath.path = path.cgPath
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
