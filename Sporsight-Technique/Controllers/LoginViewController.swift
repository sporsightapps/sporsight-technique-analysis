//
//  SignInViewController.swift
//  Sporsight-Technique
//
//  Created by Bryant Gray on 2/15/19.
//  Copyright © 2019 Sporsight. All rights reserved.
//

import UIKit
import MSAL

class LoginViewController: UIViewController {
    
    let kTenantName = "sporsightiam.onmicrosoft.com" // Your tenant name
    let kClientID = "7b66c778-b9c6-4e73-acf6-d26a969978be" // Your client ID from the portal when you created your application
    let kSigninPolicy = "b2c_1_sign_in" // Your sign-in policy you created in the portal
    let kSignupPolicy = "b2c_1_sign_up" // Your signup policy you created in the portal
    let kGraphURI = "https://sporsightiam.onmicrosoft.com/hello" // This is your backend API that you've configured to accept your app's tokens
    let kScopes: [String] = ["https://sporsightiam.onmicrosoft.com/hello/demo.read"] // This is a scope that you've configured your backend API to look for.
    
    //https://sporsightiam.onmicrosoft.com/hello/demo.read
    //https://sporsightiam.onmicrosoft.com/hello/user_impersonation
    
    // DO NOT CHANGE - This is the format of OIDC Token and Authorization endpoints for Azure AD B2C.
    let kEndpoint = "https://login.microsoftonline.com/tfp/%@/%@"
    
    var accessToken = String()
    var loggingText = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpButton(signInButton)
        setUpButton(signUpButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBOutlet weak var signInButton: UIButton!
    @IBAction func signInButtonTapped(_ sender: Any) {
        
        let kAuthority = String(format: kEndpoint, kTenantName, kSigninPolicy)
        guard let authorutyURL = URL(string: kAuthority) else {
            print("Unable to create authority URL")
            return
        }
        
        do {
            let authority = try MSALAuthority(url: authorutyURL)
            let application = try MSALPublicClientApplication.init(clientId: kClientID, authority: authority)
            
            application.acquireToken(forScopes: kScopes) { (result, error) in
                if  error == nil {
                    self.accessToken = (result?.accessToken)!
                    print("Access token is \(self.accessToken)")
                    
                    UserDefaults.standard.set(true, forKey: "isSignedIn")
                    
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    print("Could not acquire token: \(error ?? "No error informarion" as! Error)")
                }
            }
        } catch {
            print("Unable to create application \(error)")
        }
    }
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBAction func signUpButtonTapped(_ sender: Any) {
        
        let kAuthority = String(format: kEndpoint, kTenantName, kSignupPolicy)
        guard let authorutyURL = URL(string: kAuthority) else {
            print("Unable to create authority URL")
            return
        }
        
        do {
            let authority = try MSALAuthority(url: authorutyURL)
            let application = try MSALPublicClientApplication.init(clientId: kClientID, authority: authority)
            
            application.acquireToken(forScopes: kScopes) { (result, error) in
                if  error == nil {
                    self.accessToken = (result?.accessToken)!
                    print("Access token is \(self.accessToken)")
                    
                    UserDefaults.standard.set(true, forKey: "isSignedIn")
                    
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    print("Could not acquire token: \(error ?? "No error informarion" as! Error)")
                }
            }
        } catch {
            print("Unable to create application \(error)")
        }
    }
    
    //Dismisses keyboard.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
    
    @IBOutlet weak var stackView: UIStackView!
    
    func setUpButton(_ button: UIButton) {
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        button.backgroundColor = .clear
    }
}
