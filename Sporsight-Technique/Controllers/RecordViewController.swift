//
//  SecondViewController.swift
//  Sporsight-Technique
//
//  Created by Bryant Gray on 2/15/19.
//  Copyright © 2019 Sporsight. All rights reserved.
//

import UIKit
import AVFoundation
import CoreServices
import Photos

class RecordViewController: UIViewController, AVCaptureFileOutputRecordingDelegate {

    @IBOutlet weak var camPreview: UIView!
    
    @IBOutlet weak var cameraButton: UIView!
   
    @IBOutlet weak var imagePickerButton: UIView!
    
    let captureSession = AVCaptureSession()
    
    let movieOutput = AVCaptureMovieFileOutput()
    
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var activeInput: AVCaptureDeviceInput!
    
    var outputURL: URL!
    
    var isRecording = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if setupSession() {
            setupPreview()
            startSession()
        }
        
        cameraButton.isUserInteractionEnabled = true
        imagePickerButton.isUserInteractionEnabled = true
        
        let cameraButtonRecognizer = UITapGestureRecognizer(target: self, action: #selector(RecordViewController.startCapture))
        let imagePickerButtonRecognizer = UITapGestureRecognizer(target: self, action: #selector(openVideoLibrary))
        
        cameraButton.addGestureRecognizer(cameraButtonRecognizer)
        imagePickerButton.addGestureRecognizer(imagePickerButtonRecognizer)
        
        changeCameraButton()
        setUpImagePickerButton()
        
        camPreview.addSubview(cameraButton)
        camPreview.addSubview(imagePickerButton)
        
    }
    
    override func viewWillLayoutSubviews() {
        previewLayer.frame = camPreview.bounds
    }
    
    func changeCameraButton() {
        
        cameraButton.alpha = 0.8
        
        if  !isRecording {
            cameraButton.backgroundColor = UIColor.red
            cameraButton.layer.borderColor = UIColor.white.cgColor
            cameraButton.layer.borderWidth = 5
            cameraButton.layer.cornerRadius = cameraButton.bounds.height / 2
        } else {
            cameraButton.layer.borderWidth = 0
            cameraButton.layer.cornerRadius = 5
        }
        
    }
    
    func setupPreview() {
        // Configure previewLayer
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = camPreview.bounds
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        camPreview.layer.addSublayer(previewLayer)
    }
    
    //MARK:- Setup Camera
    
    func setupSession() -> Bool {
        
        captureSession.sessionPreset = AVCaptureSession.Preset.high
        
        // Setup Camera
        guard let camera = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Camera not accesible")
            return false
        }
        
        do {
            
            let input = try AVCaptureDeviceInput(device: camera)
            
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
                activeInput = input
            }
        } catch {
            print("Error setting device video input: \(error)")
            return false
        }
        
        // Setup Microphone
        let microphone = AVCaptureDevice.default(for: AVMediaType.audio)!
        
        do {
            let micInput = try AVCaptureDeviceInput(device: microphone)
            if captureSession.canAddInput(micInput) {
                captureSession.addInput(micInput)
            }
        } catch {
            print("Error setting device audio input: \(error)")
            return false
        }
        
        
        // Movie output
        if captureSession.canAddOutput(movieOutput) {
            captureSession.addOutput(movieOutput)
        }
        
        return true
    }
    
    func setupCaptureMode(_ mode: Int) {
        // Video Mode
        
    }
    
    //MARK:- Camera Session
    func startSession() {
        
        if !captureSession.isRunning {
            videoQueue().async {
                self.captureSession.startRunning()
            }
        }
    }
    
    func stopSession() {
        if captureSession.isRunning {
            videoQueue().async {
                self.captureSession.stopRunning()
            }
        }
    }
    
    func videoQueue() -> DispatchQueue {
        return DispatchQueue.main
    }
    
    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation
        
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = AVCaptureVideoOrientation.landscapeRight
        }
        
        return orientation
    }
    
    @objc func startCapture() {
        
        isRecording = !isRecording
        
        changeCameraButton()
        startRecording()
        
    }
    
    //EDIT 1: I FORGOT THIS AT FIRST
    
    func tempURL() -> URL? {
        let directory = NSTemporaryDirectory() as NSString
        
        if directory != "" {
            let path = directory.appendingPathComponent(NSUUID().uuidString + ".mp4")
            return URL(fileURLWithPath: path)
        }
        
        return nil
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        let vc = segue.destination as! VideoPlaybackViewController
//
//        vc.videoURL = sender as? URL
//
//    }
    
    func showVideoController(withURL url: URL) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "VideoPlaybackViewController") as? VideoPlaybackViewController
        vc?.videoURL = url
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    func startRecording() {
        
        if movieOutput.isRecording == false {
            
            let connection = movieOutput.connection(with: AVMediaType.video)
            
            if (connection?.isVideoOrientationSupported)! {
                connection?.videoOrientation = currentVideoOrientation()
            }
            
            if (connection?.isVideoStabilizationSupported)! {
                connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
            }
            
            let device = activeInput.device
            
            if (device.isSmoothAutoFocusSupported) {
                
                do {
                    try device.lockForConfiguration()
                    device.isSmoothAutoFocusEnabled = false
                    device.unlockForConfiguration()
                } catch {
                    print("Error setting configuration: \(error)")
                }
                
            }
            
            //EDIT2: And I forgot this
            outputURL = tempURL()
            movieOutput.startRecording(to: outputURL, recordingDelegate: self)
            
        }
        else {
            stopRecording()
        }
        
    }
    
    func stopRecording() {
        
        if movieOutput.isRecording == true {
            movieOutput.stopRecording()
        }
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
        
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        
        if (error != nil) {
            
            print("Error recording movie: \(error!.localizedDescription)")
            
        } else {
            
            let videoRecorded = outputURL! as URL
            
            print("videoRecorded: \(videoRecorded)")
            
            showVideoController(withURL: videoRecorded)
            
//            performSegue(withIdentifier: "showVideo", sender: videoRecorded)
            
        }
        
    }
    
    // MARK: - Set Up Image Picker
    
    func updateImagePickerButton() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        
        let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        let last = fetchResult.lastObject
        
        if let lastAsset = last {
            let targetSize = CGSize(width: 55, height: 55)
            let options = PHImageRequestOptions()
            options.version = .current
            
            PHImageManager.default().requestImage(
                for: lastAsset,
                targetSize: targetSize,
                contentMode: .aspectFit,
                options: options,
                resultHandler: {(image, _) in
                    DispatchQueue.main.async {
                        let imageLayer = CALayer()
                        imageLayer.frame = self.imagePickerButton.bounds
                        imageLayer.contents = image?.cgImage
                        imageLayer.masksToBounds = true
                        imageLayer.cornerRadius = 5
                        imageLayer.contentsGravity = .resizeAspectFill
                        self.imagePickerButton.layer.addSublayer(imageLayer)
                    }
            })
        }
    }
    
    func setUpImagePickerButton() {
        imagePickerButton.layer.cornerRadius = 5
        updateImagePickerButton()
    }
    
    @objc func openVideoLibrary() {
        VideoHelper.startMediaBrowser(delegate: self, sourceType: .savedPhotosAlbum)
    }
    
}

extension RecordViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL,
            UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
            else { return }
        
        dismiss(animated: true) { [weak self] in
            let videoSelected = url
            self?.showVideoController(withURL: videoSelected)
//            self?.performSegue(withIdentifier: "showVideo", sender: videoSelected)
        }
        
    }
}

extension RecordViewController: UINavigationControllerDelegate {
}
