//
//  SettingsViewController.swift
//  Sporsight-Technique
//
//  Created by Bryant Gray on 4/12/19.
//  Copyright © 2019 Sporsight. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    @IBAction func signOutTapped(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "isSignedIn")
        print(UserDefaults.standard.bool(forKey: "isSignedIn"))
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(vc, animated: true, completion: nil)
    }
    
}
