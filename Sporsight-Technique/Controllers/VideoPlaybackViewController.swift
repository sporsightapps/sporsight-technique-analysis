//
//  VideoPlayback.swift
//  Sporsight-Technique
//
//  Created by Bryant Gray on 2/15/19.
//  Copyright © 2019 Sporsight. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift
import MobileCoreServices
import ReplayKit
import SpriteKit
import Vision
import CoreML
import Upsurge
import SVProgressHUD

class VideoPlaybackViewController: UIViewController {
    
    let realm = try! Realm()
    
    let imageWidth = 368
    let imageHeight = 368
    var image : UIImage?
    

    let avPlayer = AVPlayer()
    var avPlayerLayer: AVPlayerLayer!
    var playerObserver: Any?
    var time: CMTime = CMTime.zero
    var holderView: SKView?
    var annotateView: AnnotateView?
    var toolbarList: [UIView]!
    var recordingScreen = false
    var colorIndex = 0
    var colors = [UIColor.white, UIColor.gray, UIColor.black, UIColor.red, UIColor.orange, UIColor.yellow, UIColor.green, UIColor.blue]
    
    var pose = [CAShapeLayer]()
    var keypoints = [CGPoint?]()
    var swingPlane: CAShapeLayer?
    var earlyExtensionLine: CAShapeLayer?
    var postureLine: CAShapeLayer?
    var headBoundingBox: CAShapeLayer?
    
    
    
    @IBOutlet weak var scrubber: UISlider!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cameraPosition: UISegmentedControl!
    @IBOutlet weak var handedness: UISegmentedControl!
    @IBOutlet weak var tapLabel: UILabel!
    
    @IBOutlet weak var toolbar: UIView!
    @IBOutlet weak var poseButton: UIButton!
    @IBOutlet weak var swingButton: UIButton!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var circleButton: UIButton!
    @IBOutlet weak var rectangleButton: UIButton!
    @IBOutlet weak var freehandButton: UIButton!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var finishButton: UIButton!
    
    @IBOutlet weak var annotateButton: UIButton!
    
    var videoURL: URL!
    //connect this to your uiview in storyboard
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var buttonContainer: UIView!
    @IBOutlet weak var controlsContainer: UIView!
    
    @IBOutlet weak var playButton: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override var prefersStatusBarHidden: Bool { return true }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
//        avPlayerLayer.frame = videoView.bounds
//        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoView.layer.insertSublayer(avPlayerLayer, at: 0)
        
        let interval = CMTime.init(seconds: 0.1, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        let mainQueue = DispatchQueue.main
        playerObserver = avPlayer.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: { [weak self] time in
            self?.updateScrubber(time)
        })
        
        view.layoutIfNeeded()
        
        tapLabel.isHidden = true
        tapLabel.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
//        tapLabel.layer.cornerRadius = 5
        
        scrubber.value = 0
        
        let playerItem = AVPlayerItem(url: videoURL as URL)
        avPlayer.replaceCurrentItem(with: playerItem)
        
//        saveButton.layer.backgroundColor = UIColor.black.cgColor
//        saveButton.layer.cornerRadius = 5
        
        print(videoURL)
        
        videoView.backgroundColor = .black
        
        toolbarList = [toolbar, poseButton, swingButton, arrowButton, circleButton, rectangleButton, freehandButton, recordButton, colorButton, undoButton, clearButton, finishButton]
        
        annotateButton.layer.cornerRadius = 5
        annotateButton.layer.masksToBounds = true
        annotateButton.backgroundColor = .lightGray
        annotateButton.alpha = 0.65
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideControls))
        self.videoView.addGestureRecognizer(tap)
        
        buttonContainer.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        controlsContainer.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        enableControls()
        
        handedness.selectedSegmentIndex = 1
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        avPlayer.removeTimeObserver(playerObserver)
    }
   
    @IBAction func doneButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillLayoutSubviews() {
        holderView?.frame = videoView.bounds
        avPlayerLayer.frame = videoView.bounds
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        if avPlayer.timeControlStatus == .paused {
            avPlayer.play()
            playButton.setImage(#imageLiteral(resourceName: "pauseButton"), for: .normal)
        } else {
            avPlayer.pause()
            playButton.setImage(#imageLiteral(resourceName: "playButton"), for: .normal)
        }
        
    }
    
    
    @IBAction func annotate(_ sender: Any) {
        if annotateView != nil {
            toggleToolbar()
            return
        }
        
        holderView = SKView()
        holderView?.backgroundColor = .clear
        
        guard holderView?.scene == nil else { return }
        
        annotateView = makeScene()
        holderView?.frame.size = (annotateView?.size)!
        videoView.addSubview(holderView!)
        holderView?.presentScene(annotateView!)
        annotateView?.annotationType = .freehand
        
        showToolbar()
        
        enableControls()
    }
    
    func enableControls() {
        videoView.bringSubviewToFront(annotateButton)
        videoView.bringSubviewToFront(controlsContainer)
        videoView.bringSubviewToFront(buttonContainer)
    }
    
    @objc func hideControls() {
        annotateButton.isHidden = !annotateButton.isHidden
        controlsContainer.isHidden = !controlsContainer.isHidden
        buttonContainer.isHidden = !buttonContainer.isHidden
    }
    
    func showToolbar() {
        toolbar.backgroundColor = .lightGray
        toolbar.alpha = 0.9
        toolbar.layer.cornerRadius = 10
        toolbar.layer.masksToBounds = true
        
        colorButton.layer.cornerRadius = 0.5 * colorButton.bounds.size.width
        colorButton.clipsToBounds = true
        colorIndex = 0
        colorButton.backgroundColor = colors[colorIndex]
        annotateView?.annotationColor = colors[colorIndex]
        
        arrowButton.titleLabel?.text = "A"
        circleButton.titleLabel?.text = "C"
        freehandButton.titleLabel?.text = "Fr"
        rectangleButton.titleLabel?.text = "Rt"
        recordButton.titleLabel?.text = "Rc"
        undoButton.titleLabel?.text = "U"
        finishButton.titleLabel?.text = "Fn"
        
        for item in toolbarList {
            item.isHidden = !item.isHidden
            videoView.bringSubviewToFront(item)
        }
    }
    
    func toggleToolbar() {
        for item in toolbarList {
            item.isHidden = !item.isHidden
        }
//        toolbar.isHidden = !toolbar.isHidden
    }
    
    
    @IBAction func setArrow(_ sender: Any) {
        annotateView?.annotationType = .arrow
    }
    
    @IBAction func setRectangle(_ sender: Any) {
        annotateView?.annotationType = .rectangle
    }
    
    @IBAction func setFreehand(_ sender: Any) {
        annotateView?.annotationType = .freehand
    }
    
    @IBAction func setCircle(_ sender: Any) {
        annotateView?.annotationType = .circle
    }
    
    @IBAction func undoAnnotation(_ sender: Any) {
        guard annotateView?.paths.isEmpty == false else { return }
        annotateView?.paths.last?.removeFromParent()
        annotateView?.paths.removeLast()
    }
    
    @IBAction func clearAnnotations(_ sender: Any) {
//        guard annotateView?.paths.isEmpty == false else { return }
        annotateView?.removeAllChildren()
        annotateView?.paths.removeAll(keepingCapacity: true)
        swingPlane?.removeFromSuperlayer()
        earlyExtensionLine?.removeFromSuperlayer()
        postureLine?.removeFromSuperlayer()
        headBoundingBox?.removeFromSuperlayer()
        swayLine?.removeFromSuperlayer()
        hangingBackLine?.removeFromSuperlayer()
        headBoundingBox?.removeFromSuperlayer()
        pose.forEach { (line) in
            line.removeFromSuperlayer()
        }
    }
    
    @IBAction func finishAnnotating(_ sender: Any) {
        holderView?.presentScene(nil)
        annotateView = nil
        holderView?.removeFromSuperview()
        toggleToolbar()
        
        if recordingScreen {
            let recorder = RPScreenRecorder.shared()
            
            recorder.stopRecording { [unowned self] (preview, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    self.recordingScreen = false
                } else {
                    if let unwrappedPreview = preview {
                        self.recordingScreen = false
                        unwrappedPreview.previewControllerDelegate = self
                        self.present(unwrappedPreview, animated: true)
                    }
                }
            }
        }
    }
    
    
    @IBAction func screenRecord(_ sender: Any) {
        let recorder = RPScreenRecorder.shared()
        
        if recordingScreen == false {
            recorder.startRecording { [unowned self] (error) in
                if error != nil {
                    print(error!.localizedDescription)
                } else {
                    self.recordingScreen = true
                }
            }
        } else {
            recorder.stopRecording { [unowned self] (preview, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    self.recordingScreen = false
                } else {
                    if let unwrappedPreview = preview {
                        self.recordingScreen = false
                        unwrappedPreview.previewControllerDelegate = self
                        self.present(unwrappedPreview, animated: true)
                    }
                }
            }
        }
    }
    
    @IBAction func changeAnnotationColor(_ sender: Any) {
        if colorIndex + 1 >= colors.count {
            colorIndex = 0
        } else {
            colorIndex += 1
        }
        
        colorButton.backgroundColor = colors[colorIndex]
        annotateView?.annotationColor = colors[colorIndex]
    }
    
    
    
    @IBAction func swingButtonTapped(_ sender: Any) {
        let image = getImageFromVideo(url: videoURL, at: avPlayer.currentTime())
        
        getKeypointsAndPose(image: image)

        if cameraPosition.selectedSegmentIndex == 1 {
            // Behind camera angle
            toggleToolbar()
            tapLabel.isHidden = false
            var tapGestureRecongnizer: UITapGestureRecognizer!
            tapGestureRecongnizer = UITapGestureRecognizer(target: self, action: #selector(self.ballTapped(touch:)))
            videoView.addGestureRecognizer(tapGestureRecongnizer)
        } else {
            // Side camera angle
            let leftAnkle = keypoints[CocoPart.LAnkle.rawValue]
            let rightAnkle = keypoints[CocoPart.RAnkle.rawValue]
            let leftShoulder = keypoints[CocoPart.LShoulder.rawValue]
            let rightShoulder = keypoints[CocoPart.RShoulder.rawValue]
            let hipPoint: CGPoint?
            
            if swayLine != nil || hangingBackLine != nil || headBoundingBox != nil {
                swayLine?.removeFromSuperlayer()
                hangingBackLine?.removeFromSuperlayer()
                headBoundingBox?.removeFromSuperlayer()
            }
            
            if handedness.selectedSegmentIndex == 1 {
                // Right handed
                hipPoint = keypoints[CocoPart.RHip.rawValue]
                addSwayLine(ankle: rightAnkle!, hip: hipPoint!, color: .white)
                addHangingBackLine(ankle: leftAnkle!, color: .white)
                addHeadBoundingBox(shoulder1: rightShoulder!, shoulder2: leftShoulder!, color: .white)
            } else {
                // Left handed
                hipPoint = keypoints[CocoPart.LHip.rawValue]
                addSwayLine(ankle: leftAnkle!, hip: hipPoint!, color: .white)
                addHangingBackLine(ankle: rightAnkle!, color: .white)
                addHeadBoundingBox(shoulder1: leftShoulder!, shoulder2: rightShoulder!, color: .white)
            }
            
            videoView.layer.addSublayer(swayLine!)
            videoView.layer.addSublayer(hangingBackLine!)
            videoView.layer.addSublayer(headBoundingBox!)
        }
        
        SVProgressHUD.dismiss()
    }
    
    // MARK: - Behind Camera View
    
    @objc func ballTapped(touch: UITapGestureRecognizer) {
        let touchpoint = touch.location(in: self.view)
        
//        let firstPoint = pose[2].path?.firstPoint()
        let elbowPoint: CGPoint
        let hipPoint = keypoints[CocoPart.MidHip.rawValue]
        let nosePoint = keypoints[CocoPart.Nose.rawValue]
        let neckPoint = keypoints[CocoPart.Neck.rawValue]
        var secondPoint: CGPoint?
        
        if keypoints[CocoPart.RElbow.rawValue] != nil {
            elbowPoint = keypoints[CocoPart.RElbow.rawValue]!
        } else {
            //
            var point = CGPoint()
            point.x = (neckPoint!.x + hipPoint!.x) / 2
            point.y = (neckPoint!.y + hipPoint!.y) / 2
            elbowPoint = point
        }
        
        guard let _ = secondPoint else {
            secondPoint = touchpoint
            
            if swingPlane == nil {
                addSwingPlane(fromPoint: elbowPoint, toPoint: secondPoint!, color: .red)
                videoView.layer.addSublayer(swingPlane!)
            } else {
                swingPlane?.removeFromSuperlayer()
                addSwingPlane(fromPoint: elbowPoint, toPoint: secondPoint!, color: .red)
                videoView.layer.addSublayer(swingPlane!)
            }
            
            if earlyExtensionLine == nil {
                addEarlyExstensionLine(hipPoint: hipPoint!, color: .white)
                videoView.layer.addSublayer(earlyExtensionLine!)
            } else {
                earlyExtensionLine?.removeFromSuperlayer()
                addEarlyExstensionLine(hipPoint: hipPoint!, color: .white)
                videoView.layer.addSublayer(earlyExtensionLine!)
            }
            
            if postureLine == nil && headBoundingBox == nil {
                addPostureLine(nose: nosePoint!, neck: neckPoint!, hip: hipPoint!, color: .white)
                videoView.layer.addSublayer(postureLine!)
                videoView.layer.addSublayer(headBoundingBox!)
            } else {
                postureLine?.removeFromSuperlayer()
                headBoundingBox?.removeFromSuperlayer()
                addPostureLine(nose: nosePoint!, neck: neckPoint!, hip: hipPoint!, color: .white)
                videoView.layer.addSublayer(postureLine!)
                videoView.layer.addSublayer(headBoundingBox!)
            }
            
            pose.removeAll()
            
            tapLabel.isHidden = true
            
//            touch.removeTarget(self, action: nil)
            videoView.removeGestureRecognizer(touch)
            
            toggleToolbar()
            
            return
        }
    }
    
    func addSwingPlane(fromPoint start: CGPoint, toPoint end: CGPoint, color: UIColor) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        let extendedLinePoint = CGPoint(x: start.x - (end.x - start.x), y: start.y - (end.y - start.y) + 30)
        linePath.move(to: extendedLinePoint)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = color.withAlphaComponent(0.6).cgColor
        line.lineWidth = 5
        line.lineJoin = CAShapeLayerLineJoin.round
        line.lineCap = .round
        swingPlane = line
    }
    
    func addEarlyExstensionLine(hipPoint: CGPoint, color: UIColor) {
        let lineOffset: CGFloat = -20
        
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        let start = CGPoint(x: hipPoint.x + lineOffset, y: hipPoint.y - 100)
        let end = CGPoint(x: start.x, y: hipPoint.y + 100)
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = color.withAlphaComponent(0.6).cgColor
        line.lineWidth = 5
        line.lineJoin = CAShapeLayerLineJoin.round
        line.lineCap = .round
        earlyExtensionLine = line
    }
    
    func addPostureLine(nose: CGPoint, neck: CGPoint, hip: CGPoint, color: UIColor) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: neck)
        linePath.addLine(to: hip)
        line.path = linePath.cgPath
        line.strokeColor = color.withAlphaComponent(0.6).cgColor
        line.lineWidth = 5
        line.lineCap = .round
        postureLine = line
        
        let box = CAShapeLayer()
        var boxWidth = keypoints[CocoPart.Neck.rawValue]!.x - keypoints[CocoPart.Nose.rawValue]!.x
        var boxHeight = keypoints[CocoPart.Neck.rawValue]!.y - keypoints[CocoPart.Nose.rawValue]!.y
        boxWidth = abs(boxWidth) * 2
        boxHeight = abs(boxHeight) * 2
        let boxOrigin = CGPoint(x: neck.x, y: neck.y - boxWidth + 10)
        let boxSize = CGSize(width: boxWidth, height: boxWidth)
        let boxRect = CGRect(origin: boxOrigin, size: boxSize)
        let boxPath = UIBezierPath(roundedRect: boxRect, cornerRadius: 5)
        box.path = boxPath.cgPath
        box.strokeColor = color.withAlphaComponent(0.6).cgColor
        box.lineWidth = 5
        box.fillColor = UIColor.clear.cgColor
        headBoundingBox = box
    }
    
    // MARK: - Side Camera View
    var swayLine: CAShapeLayer?
    func addSwayLine(ankle: CGPoint, hip: CGPoint, color: UIColor) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: hip)
        linePath.addLine(to: ankle)
        line.path = linePath.cgPath
        line.strokeColor = color.withAlphaComponent(0.6).cgColor
        line.lineWidth = 5
        line.lineCap = .round
        swayLine = line
    }
    
    var hangingBackLine: CAShapeLayer?
    func addHangingBackLine(ankle: CGPoint, color: UIColor) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        let neck = keypoints[CocoPart.Neck.rawValue]
        linePath.move(to: ankle)
        linePath.addLine(to: CGPoint(x: ankle.x, y: neck!.y))
        line.path = linePath.cgPath
        line.strokeColor = color.withAlphaComponent(0.6).cgColor
        line.lineWidth = 5
        line.lineCap = .round
        hangingBackLine = line
    }
    
    func addHeadBoundingBox(shoulder1: CGPoint, shoulder2: CGPoint, color: UIColor) {
        let box = CAShapeLayer()
        let shoulderDistance = keypoints[CocoPart.LShoulder.rawValue]!.x - keypoints[CocoPart.RShoulder.rawValue]!.x
        let shoulderHeight = (keypoints[CocoPart.LShoulder.rawValue]!.y + keypoints[CocoPart.RShoulder.rawValue]!.y) / 2
        let boxWidth = abs(shoulderDistance) / 1.5
        let boxOrigin = CGPoint(x: shoulder1.x + (0.125 * shoulderDistance), y: shoulderHeight - (boxWidth))
        let boxSize = CGSize(width: boxWidth, height: boxWidth)
        let boxRect = CGRect(origin: boxOrigin, size: boxSize)
        let boxPath = UIBezierPath(roundedRect: boxRect, cornerRadius: 5)
        box.path = boxPath.cgPath
        box.strokeColor = color.withAlphaComponent(0.6).cgColor
        box.lineWidth = 5
        box.fillColor = UIColor.clear.cgColor
        headBoundingBox = box
    }
    
    // MARK: - PREVIOUS STUFF
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Save Video", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Save", style: .default) { [weak self] (action) in
            
            if let videoName = textField.text {
                self?.saveVideo(named: videoName)
            }
        }
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Video name..."
            textField = alertTextField
        }
        
        alert.addAction(action)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func saveVideo(named videoName: String = "Untitled") {
        
        let date = Date()
        let videoData = NSData(contentsOf: videoURL)
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0]
        let dataPath = documentsDirectory + "/\(videoName)-\(date.description).mp4"
        
        videoData?.write(toFile: dataPath, atomically: false)
        
        let newVideo = Video()
        newVideo.title = videoName
        newVideo.url = dataPath
        newVideo.dateCreated = date
        
        do {
            try realm.write {
                realm.add(newVideo)
            }
        } catch {
            print("Error saving video, \(error)")
        }
        
        print(dataPath)
    }
    
//    func randomString(length: Int) -> String {
//        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
//        return String((0...length-1).map { _ in letters.randomElement()! })
//    }
    
    @IBAction func scrubberTouchBegan(_ sender: Any) {
        avPlayer.pause()
        playButton.setImage(#imageLiteral(resourceName: "playButton"), for: .normal)
    }
    @IBAction func scrubberMoved(_ sender: Any) {
        handleScrubberChange()
    }
    
    func updateScrubber(_ time: CMTime) {
        let duration = avPlayer.currentItem?.asset.duration
        let current = avPlayer.currentTime()
        let totalSeconds = CMTimeGetSeconds(duration!)
        let currentSeconds = CMTimeGetSeconds(current)
        
        scrubber.setValue(Float(currentSeconds / totalSeconds), animated: true)
        
        if current == duration {
            playButton.setImage(#imageLiteral(resourceName: "playButton"), for: .normal)
        }
    }
    
    func handleScrubberChange() {
        
        if let duration = avPlayer.currentItem?.duration {
            let totalSeconds = CMTimeGetSeconds(duration)
            
            let value = Float64(scrubber.value) * totalSeconds
            
            let seekTime = CMTime(seconds: value, preferredTimescale: 100)
            time = seekTime
            
            avPlayer.seek(to: seekTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        }
    }
    
    // MARK: - Pose Estimation
    
    
    @IBAction func poseButtonTapped(_ sender: Any) {
        let image = getImageFromVideo(url: videoURL, at: avPlayer.currentTime())
        
        for line in pose {
            line.removeFromSuperlayer()
        }
        
        getKeypointsAndPose(image: image)
        
        for line in pose {
            videoView.layer.addSublayer(line)
        }
    }
    
    let poseEstimator = Pose()
    func getKeypointsAndPose(image: UIImage?) {
        
//        let poseEstimator = Pose(url: videoURL)

        poseEstimator.runImagePoseEstimation(image: image!)
        let pos = poseEstimator.getPose()
        let unscaledKeypoints = poseEstimator.getKeypoints()
        
        let scaledImageSize = AVMakeRect(aspectRatio: image!.size, insideRect: videoView.bounds)
        
        var points = [CGPoint?]()
        
        pose.forEach { (line) in
            line.removeFromSuperlayer()
        }
        
        self.pose.removeAll()
        
        for p in pos {
            var point = CGPoint()
            
            if p != nil {
                point.x = p!.x * scaledImageSize.width + ((videoView.bounds.width - scaledImageSize.width) / 2)
                point.y = p!.y * scaledImageSize.height + ((videoView.bounds.height - scaledImageSize.height) / 2)
                points.append(point)
            } else {
                points.append(nil)
            }
        }
        
        self.keypoints.removeAll()
        
        for keypoint in unscaledKeypoints {
            
            var point = CGPoint()
            
            if keypoint != nil {
                point.x = keypoint!.x * scaledImageSize.width + ((videoView.bounds.width - scaledImageSize.width) / 2)
                point.y = keypoint!.y * scaledImageSize.height + ((videoView.bounds.height - scaledImageSize.height) / 2)
                keypoints.append(point)
            } else {
                keypoints.append(nil)
            }
        }
        
        for var i in 0..<points.count/2 {
            if points[i*2] != nil && points[i*2+1] != nil {
                addLine(fromPoint: points[i*2]!, toPoint: points[i*2+1]!, color: .lightGray)
            } else {
                i += 2
            }
            
        }
    }
        
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.height
        //        let scale = image.size.width / newWidth
        print("width: \(image.size.width)\theight: \(image.size.height)")
        print("scale: \(scale)")
        let newHeight = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func addLine(fromPoint start: CGPoint, toPoint end: CGPoint, color: UIColor) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = color.withAlphaComponent(0.6).cgColor
        line.lineWidth = 5
        line.lineJoin = CAShapeLayerLineJoin.round
        line.lineCap = .round
        pose.append(line)
    }
    
    func getImageFromVideo(url: URL, at time: CMTime) -> UIImage? {
        let asset = AVURLAsset(url: url)
        
        let assetIG = AVAssetImageGenerator(asset: asset)
        assetIG.requestedTimeToleranceBefore = CMTime.zero
        assetIG.requestedTimeToleranceAfter = CMTime.zero
        assetIG.appliesPreferredTrackTransform = true
        assetIG.apertureMode = AVAssetImageGenerator.ApertureMode.encodedPixels
        
        let imageRef: CGImage
        
        do {
            imageRef = try assetIG.copyCGImage(at: time, actualTime: nil)
        } catch let error {
            print("Error: \(error)")
            return nil
        }
        return UIImage(cgImage: imageRef)
    }
}

extension VideoPlaybackViewController {
    func makeScene() -> AnnotateView {
//        let minimumDimension = min(view.frame.width, view.frame.height)

//        let size = CGSize(width: minimumDimension, height: minimumDimension)
        
        let size = CGSize(width: videoView.frame.width, height: view.frame.height)
        
        let scene = AnnotateView(size: size)
        scene.backgroundColor = .clear
        return scene
    }
}

extension VideoPlaybackViewController: RPPreviewViewControllerDelegate {
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true)
    }
}

extension VideoPlaybackViewController: UINavigationControllerDelegate {
}

extension UIImage {
    func resize(to newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: newSize.width, height: newSize.height), true, 1.0)
        draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return resizedImage
    }
}

extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0
        
        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}

// rob mayoff's CGPath.foreach
//extension CGPath {
//    func forEach( body: @escaping @convention(block) (CGPathElement) -> Void) {
//        typealias Body = @convention(block) (CGPathElement) -> Void
//        func callback(info: UnsafeMutableRawPointer?, element: UnsafePointer<CGPathElement>) {
//            let body = unsafeBitCast(info, to: Body.self)
//            body(element.pointee)
//        }
//        let unsafeBody = unsafeBitCast(body, to: UnsafeMutableRawPointer.self)
//        self.apply(info: unsafeBody, function: callback)
//    }

//    func firstPoint() -> CGPoint? {
//        var firstPoint: CGPoint? = nil
//
//        self.forEach { element in
//
//            guard firstPoint == nil else { return }
//            assert(element.type == .moveToPoint , "Expected the first point to be a move")
//            firstPoint = element.points.pointee
//        }
//        return firstPoint
//    }
//}

// Finds the first point in a path
//extension UIBezierPath {
//    func firstPoint() -> CGPoint? {
//        var firstPoint: CGPoint? = nil
//
//        self.cgPath.forEach { element in
//            // Just want the first one, but we have to look at everything
//            guard firstPoint == nil else { return }
//            assert(element.type == .moveToPoint, "Expected the first point to be a move")
//            firstPoint = element.points.pointee
//        }
//        return firstPoint
//    }
//}
