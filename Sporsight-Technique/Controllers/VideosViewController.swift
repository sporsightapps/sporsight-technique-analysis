//
//  VideosTableViewController.swift
//  Sporsight-Technique
//
//  Created by Bryant Gray on 2/18/19.
//  Copyright © 2019 Sporsight. All rights reserved.
//

import UIKit
import RealmSwift
import AVFoundation
import AVKit
import Alamofire
import SwiftyJSON

class VideosViewController: UITableViewController {
    
    let realm = try! Realm()
    
    var videos: Results<Video>?
    var cloudVideos = [[String:AnyObject]]()
    
    var videoURL = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        tableView.separatorStyle = .none
        navigationItem.title = "Videos"
        
        tableView.sectionIndexBackgroundColor = .rgb(44, 75, 111)
        loadVideos()
        
        getContainerVideos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .rgb(44, 75, 111)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return videos?.count ?? 1
        } else {
            return cloudVideos.count // get count of container vids
        }
        
//        return cloudVideos.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Local"
        } else {
            return "Cloud"
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! videoTableViewCell
        
        if indexPath.section == 0 {
            let video = videos?[indexPath.row]
            
            cell.titleLabel.text = video?.title ?? "No videos saved yet."
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            
            dateFormatter.locale = Locale(identifier: "en_US")
            
            if let date = video?.dateCreated {
                cell.dateLabel.text = dateFormatter.string(from: date)
            } else {
                cell.dateLabel.text = ""
            }
            
            if let url = video?.url {
                cell.videoImage.image = getVideoSnapshot(url: url)
            }
        
        } else {
            var dict = cloudVideos[indexPath.row]
            cell.titleLabel.text = dict["name"] as? String
            
            
            let dfGet = DateFormatter()
            dfGet.locale = Locale(identifier: "en_US")
            dfGet.dateFormat = "E, dd MMM yyyy HH:mm:ss zzz"
            
            let unformattedDate = dict["date"] as? String
            
            let dfPrint = DateFormatter()
            dfPrint.locale = Locale(identifier: "en_US")
            dfPrint.dateStyle = .medium
            
            if let date = dfGet.date(from: unformattedDate!) {
                cell.dateLabel.text = dfPrint.string(from: date)
            }
        }
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func getVideoSnapshot(url: String) -> UIImage? {
        
        let vidURL = URL(fileURLWithPath: url)
        let asset = AVURLAsset(url: vidURL)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        
        let timestamp = CMTime(seconds: 1, preferredTimescale: 60)
        
        do {
            let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("Image generation failed, \(error)")
            return nil
        }
    }
 

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
 

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if editingStyle == .delete {
                // Delete the row from the data source
                if let videoForDeletion = videos?[indexPath.row] {
                    do{
                        try realm.write {
                            realm.delete(videoForDeletion)
                        }
                    } catch {
                        print("Error deleting video, \(error)")
                    }
                }
                tableView.deleteRows(at: [indexPath], with: .fade)
            } else if editingStyle == .insert {
                // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
            }
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            let selectedVideo = videos?[indexPath.row].url
            let selectedURL = URL(fileURLWithPath: selectedVideo!)
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "VideoPlaybackViewController") as! VideoPlaybackViewController
            
            vc.videoURL = selectedURL
            
            
            self.present(vc, animated: true, completion: nil)
        } else {
            let videoName = cloudVideos[indexPath.row]["name"] as! String
            playVideo(videoName: videoName, containerName: "swings")
        }
        
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if let indexPath = tableView.indexPathForSelectedRow {
            let selectedVideo = videos?[indexPath.row].url
            
            let selectedURL = URL(fileURLWithPath: selectedVideo!)
            
            let vc = segue.destination as! VideoPlaybackViewController
            
            vc.videoURL = selectedURL
        }
    }
 
    
    // MARK: - Data Manipulation Methods
    
    func loadVideos() {
        videos = realm.objects(Video.self)
        
        tableView.reloadData()
    }
    
    // MARK: Networking
    
    func getContainerVideos() {
        
        Alamofire.request("https://sporsight-api.azurewebsites.net/videos/get_blobs/swings").responseJSON { [unowned self] (response) in

            if response.result.value != nil {
                let json = JSON(response.result.value!)
                
                if let resData = json["videos"].arrayObject {
                    self.cloudVideos = resData as! [[String:AnyObject]]
                }
                
                if self.cloudVideos.count > 0 {
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    func playVideo(videoName: String, containerName: String) {
        
        Alamofire.request("https://sporsight-api.azurewebsites.net/videos/get_blob/\(containerName)&\(videoName)").responseJSON { [unowned self] (response) in
            if response.result.value != nil {
                let json = JSON(response.result.value!)
                
                if let resData = json["sasUrl"].string {
                    self.videoURL = resData
                }
            }
            
            let url = URL(string: self.videoURL)
            let player = AVPlayer(url: url!)
            
            let playerVC = AVPlayerViewController()
            playerVC.player = player
            
            self.present(playerVC, animated: true, completion: nil)
            
        }
    }

}

class videoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
}
