//extension CGPoint {
//    init(_ x: CGFloat, _ y: CGFloat) {
//        self.x = x
//        self.y = y
//    }
//}
extension UIColor {
    class func rgb(_ r: Int,_ g: Int,_ b: Int) -> UIColor{
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1)
    }
}

//enum CocoPart: Int {
//    case Nose = 0
//    case Neck = 1
//    case RShoulder = 2
//    case RElbow = 3
//    case RWrist = 4
//    case LShoulder = 5
//    case LElbow = 6
//    case LWrist = 7
//    case RHip = 8
//    case RKnee = 9
//    case RAnkle = 10
//    case LHip = 11
//    case LKnee = 12
//    case LAnkle = 13
//    case REye = 14
//    case LEye = 15
//    case REar = 16
//    case LEar = 17
//    case Background = 18
//}

enum CocoPart: Int {
    case Nose = 0
    case Neck = 1
    case RShoulder = 2
    case RElbow = 3
    case RWrist = 4
    case LShoulder = 5
    case LElbow = 6
    case LWrist = 7
    case MidHip = 8
    case RHip = 9
    case RKnee = 10
    case RAnkle = 11
    case LHip = 12
    case LKnee = 13
    case LAnkle = 14
    case REye = 15
    case LEye = 16
    case REar = 17
    case LEar = 18
    case LBigToe = 19
    case LSmallToe = 20
    case LHeel = 21
    case RBigToe = 22
    case RSmallToe = 23
    case RHeel = 24
    case Background = 25
}

//let CocoPairs = [
//    (1, 2), (1, 5), (2, 3), (3, 4), (5, 6), (6, 7), (1, 8), (8, 9), (9, 10), (1, 11),
//    (11, 12), (12, 13), (1, 0), (0, 14), (14, 16), (0, 15), (15, 17), (2, 16), (5, 17)
//]

let CocoPairs = [
    (1, 8), (1, 2), (1, 5), (2, 3), (3, 4), (5, 6), (6, 7), (8, 9), (9, 10), (10, 11),
    (8, 12), (12, 13), (13, 14), (1, 0), (0, 15), (15, 17), (0, 16), (16, 18), (14, 19), (19,20), (14, 21), (11, 22), (22, 23), (11, 24)
]
let CocoPairsRender = CocoPairs[0..<CocoPairs.count-2]
//let CocoPairsNetwork = [
//(12, 13), (20, 21), (14, 15), (16, 17), (22, 23), (24, 25), (0, 1), (2, 3), (4, 5),
//(6, 7), (8, 9), (10, 11), (28, 29), (30, 31), (34, 35), (32, 33), (36, 37), (18, 19), (26, 27)
//]

let CocoPairsNetwork = [
    (0, 1), (14, 15), (22, 23), (16, 17), (18, 19), (24, 25), (26, 27), (6, 7), (2, 3),
    (4, 5), (8, 9), (10, 11), (12, 13), (30, 31), (32, 33), (36, 37), (34, 35), (38, 39), (40, 41), (42, 43), (44, 45), (46, 47), (48, 49), (50, 51)
]

//let CocoColors = [UIColor.rgb(255, 0, 0),  UIColor.rgb(255, 85, 0), UIColor.rgb(255, 170, 0),UIColor.rgb(255, 255, 0),
//                  UIColor.rgb(170, 255, 0),UIColor.rgb(85, 255, 0), UIColor.rgb(0, 255, 0),  UIColor.rgb(0, 255, 85),
//                  UIColor.rgb(0, 255, 170),UIColor.rgb(0, 255, 255),UIColor.rgb(0, 170, 255),UIColor.rgb(0, 85, 255),
//                  UIColor.rgb(0, 0, 255),  UIColor.rgb(85, 0, 255), UIColor.rgb(170, 0, 255),UIColor.rgb(255, 0, 255),
//                  UIColor.rgb(255, 0, 170),UIColor.rgb(255, 0, 85)]

let CocoColors = [UIColor.rgb(255, 0, 0),  UIColor.rgb(255, 85, 0), UIColor.rgb(255, 170, 0),UIColor.rgb(255, 255, 0),
                  UIColor.rgb(170, 255, 0),UIColor.rgb(85, 255, 0), UIColor.rgb(0, 255, 0),  UIColor.rgb(0, 255, 85),
                  UIColor.rgb(0, 255, 170),UIColor.rgb(0, 255, 255),UIColor.rgb(0, 170, 255),UIColor.rgb(0, 85, 255),
                  UIColor.rgb(0, 0, 255),  UIColor.rgb(85, 0, 255), UIColor.rgb(170, 0, 255),UIColor.rgb(255, 0, 255),
                  UIColor.rgb(255, 0, 170),UIColor.rgb(255, 0, 85), UIColor.rgb(255, 0, 0),  UIColor.rgb(255, 85, 0),
                  UIColor.rgb(255, 170, 0),UIColor.rgb(255, 255, 0),UIColor.rgb(170, 255, 0),UIColor.rgb(85, 255, 0),
                  UIColor.rgb(0, 255, 0)]

func resizeImage(image: UIImage, size: CGSize, keepAspectRatio: Bool = false, useToMakeVideo: Bool = false) -> UIImage {
    var targetSize: CGSize = size
    
    if useToMakeVideo {
        // Resize width to a multiple of 16.
        let resizeRate: CGFloat = CGFloat(Int(image.size.width) / 16) * 16 / image.size.width
        targetSize = CGSize(width: image.size.width * resizeRate, height: image.size.height * resizeRate)
    }
    
    var newSize: CGSize = targetSize
    var newPoint: CGPoint = CGPoint(x: 0, y: 0)
    
    if keepAspectRatio {
        if targetSize.width / image.size.width <= targetSize.height / image.size.height {
            newSize = CGSize(width: targetSize.width, height: image.size.height * targetSize.width / image.size.width)
            newPoint.y = (targetSize.height - newSize.height) / 2
        } else {
            newSize = CGSize(width: image.size.width * targetSize.height / image.size.height, height: targetSize.height)
            newPoint.x = (targetSize.width - newSize.width) / 2
        }
    }
    
    UIGraphicsBeginImageContext(targetSize)
    image.draw(in: CGRect(x: newPoint.x, y: newPoint.y, width: newSize.width, height: newSize.height))
    let resizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    
    return resizedImage
}
