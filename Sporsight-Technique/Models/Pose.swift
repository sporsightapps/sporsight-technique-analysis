//
//  VideoPose.swift
//  Sporsight-Technique
//
//  Created by Bryant Gray on 3/27/19.
//  Copyright © 2019 Sporsight. All rights reserved.
//

import Foundation
import AVFoundation
import Vision
import CoreML

class Pose {
    
//    init(url: URL) {
//        self.videoUrl = url
//    }
    
//    let MLmodel = body_25()
    let imageWidth = 368
    let imageHeight = 368
    
//    let videoUrl: URL
    
    private var frames = [UIImage]()
    private var pose = [(CMTime ,[CGPoint?])]()
    private var keypoints = [(CMTime ,[CGPoint?])]()
    private var imageKeypoints = [CGPoint?]()
    private var imagePose = [CGPoint?]()
    
    private var time = CMTime()
    
    // Computes pose for whole video
//    private func getAllFrames() {
//
//        let asset = AVAsset(url: videoUrl)
//        let generator = AVAssetImageGenerator(asset: asset)
//
//        var frameForTimes = [NSValue]()
//
//        let reader = try! AVAssetReader(asset: asset)
//
//        let videoTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
//
//        // read video frames as BGRA
//        let trackReaderOutput = AVAssetReaderTrackOutput(track: videoTrack, outputSettings:[String(kCVPixelBufferPixelFormatTypeKey): NSNumber(value: kCVPixelFormatType_32BGRA)])
//
//        reader.add(trackReaderOutput)
//        reader.startReading()
//
//        while let sampleBuffer = trackReaderOutput.copyNextSampleBuffer() {
//            if let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) {
//                // images as CVImageBuffers/CVPixelBuffers
//                let frameTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
//                frameForTimes.append(NSValue(time: frameTime))
//            }
//        }
//
//        let duration = asset.duration
//        generator.appliesPreferredTrackTransform = true
//        generator.requestedTimeToleranceBefore = CMTime.zero
//        generator.requestedTimeToleranceAfter = CMTime.zero
//
//        generator.generateCGImagesAsynchronously(forTimes: frameForTimes) { (requestedTime, image, actualTime, result, error) in
//            if let image = image {
////                print(requestedTime.value, requestedTime.seconds, actualTime.value)
////                self.frames.append(UIImage(cgImage: image))
//                self.time = actualTime
////                self.getKeypoints(image: UIImage(cgImage: image))
//                print("progress: \(self.time.seconds / duration.seconds)")
//            }
//        }
//    }
    
    func runImagePoseEstimation(image: UIImage) {
        runCoreML(image)
    }
    
    func getKeypoints() -> [CGPoint?] {
        return imageKeypoints
    }
    
    func getPose() -> [CGPoint?] {
        return imagePose
    }
    
    private func measure <T> (_ f: @autoclosure () -> T) -> (result: T, duration: String) {
        let startTime = CFAbsoluteTimeGetCurrent()
        let result = f()
        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        return (result, "Elapsed time is \(timeElapsed) seconds.")
    }
    
    lazy var classificationRequest: [VNRequest] = {
        do {
            let model = try VNCoreMLModel(for: body_25().model)
            let classificationRequest = VNCoreMLRequest(model: model, completionHandler: { [weak self] (request, error) in
                self?.handleClassification(request: request, error: error)
            })
            return [ classificationRequest ]
        } catch {
            fatalError("Can't load Vision ML model: \(error)")
        }
    }()
    
    private func handleClassification(request: VNRequest, error: Error?) {
        
        guard let observations = request.results as? [VNCoreMLFeatureValueObservation] else { fatalError() }
        let mlarray = observations[0].featureValue.multiArrayValue!
        let length = mlarray.count
        let doublePtr = mlarray.dataPointer.bindMemory(to: Double.self, capacity: length)
        let doubleBuffer = UnsafeBufferPointer(start: doublePtr, count: length)
        let mm = Array(doubleBuffer)
        
        getPoseEstimation(mm)
    }
    
   private func runCoreML(_ image: UIImage) {
        
        let img = image.resize(to: CGSize(width: imageWidth, height: imageHeight)).cgImage!
        let classificationRequestHandler = VNImageRequestHandler(cgImage: img, options: [:])
        do {
            try classificationRequestHandler.perform(self.classificationRequest)
        } catch {
            print(error)
        }
    }
    
    private func getPoseEstimation(_ mm: [Double]) {
        
        let com = PoseEstimator(imageWidth, imageHeight)
        
        let res = measure(com.estimate(mm))
//        let res = com.estimate(mm)
//        let humans = res.result
        let human = res.result[0]
//        print("estimate \(res.duration)")
        
        var pos = [CGPoint?]()
//        var unscaledKeypoints = [CGPoint?]()
        
        var centers = [Int : CGPoint?]()
        for i in 0...CocoPart.Background.rawValue {
            if human.bodyParts.keys.index(of: i) == nil {
                centers.updateValue(nil, forKey: i)
                continue
            }
            let bodyPart = human.bodyParts[i]!
            centers.updateValue(CGPoint(x: bodyPart.x, y: bodyPart.y), forKey: i)
        }
        
        var points = [CGPoint?]()
        for i in 0..<centers.count {
            let point = centers[i]
            points.append(point!)
        }
        
        self.imageKeypoints = points
        
        self.keypoints.append((time, points))
        
        for (_, (pair1, pair2)) in CocoPairsRender.enumerated() {
            pos.append(centers[pair1]!)
            pos.append(centers[pair2]!)
        }
        
        self.imagePose = pos
        
        self.pose.append((time, pos))
        
//        points.removeAll()
//        for p in pos {
//            var point = CGPoint()
//
//            if p != nil {
//                point.x = p!.x
//                point.y = p!.y
//                self.pose.append(point)
//            } else {
//                self.pose.append(nil)
//            }
//        }
        
//        keypoints.removeAll()
//
//        for keypoint in unscaledKeypoints {
//
//            var point = CGPoint()
//
//            if keypoint != nil {
//                point.x = keypoint!.x
//                point.y = keypoint!.y
//                keypoints.append(point)
//            } else {
//                keypoints.append(nil)
//            }
//        }
        
    }
}
