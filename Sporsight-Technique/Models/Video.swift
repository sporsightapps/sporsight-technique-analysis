//
//  Video.swift
//  Sporsight-Technique
//
//  Created by Bryant Gray on 2/18/19.
//  Copyright © 2019 Sporsight. All rights reserved.
//

import Foundation
import RealmSwift

class Video: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var dateCreated: Date?
}
